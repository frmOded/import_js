// test.js
let foundDirname = typeof __dirname === "string";
let foundModule = typeof module === "object";

console.log("found __dirname", foundDirname);
console.log("found module", foundModule);

if (foundDirname && foundModule) {
    console.log("appears to be a CommonJS module")
} else {
    console.log("appears to be an ESM module");
}
